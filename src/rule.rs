use neighborhood::Neighborhood;

trait Rule<C> {
    fn evaluate(&mut self, center: C, neighborhood: Neighborhood<C>) -> C;
}

pub struct Const<C> {
    pub value: C,
}

impl<C> Rule<C> for Const<C>
    where C: Clone
{
    fn evaluate(&mut self, _: C, _: Neighborhood<C>) -> C {
        self.value.clone()
    }
}

pub struct Id;

impl<C> Rule<C> for Id {
    fn evaluate(&mut self, center: C, _: Neighborhood<C>) -> C {
        center
    }
}

impl<C, F> Rule<C> for F
    where F: FnMut(C, Neighborhood<C>) -> C
{
    fn evaluate(&mut self, center: C, neighborhood: Neighborhood<C>) -> C {
        self(center, neighborhood)
    }
}
