#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub struct Neighborhood<C> {
    pub top: C,
    pub top_right: C,
    pub right: C,
    pub bottom_right: C,
    pub bottom: C,
    pub bottom_left: C,
    pub left: C,
    pub top_left: C,
}

#[derive(Debug)]
pub struct Iter<'n, C: 'n> {
    neighborhood: &'n Neighborhood<C>,
    current: u8,
}

impl<'n, C: 'n> Iterator for Iter<'n, C>
{
    type Item = &'n C;

    fn next(&mut self) -> Option<Self::Item> {
        let neighborhood = self.neighborhood;
        let item = match self.current {
            0 => Some(&neighborhood.top),
            1 => Some(&neighborhood.top_right),
            2 => Some(&neighborhood.right),
            3 => Some(&neighborhood.bottom_right),
            4 => Some(&neighborhood.bottom),
            5 => Some(&neighborhood.bottom_left),
            6 => Some(&neighborhood.left),
            7 => Some(&neighborhood.top_left),
            _ => None,
        };
        self.current += 1;
        item
    }
}
